CREATE TABLE "profile" (
  "id" serial,
  "name" varchar(255) COLLATE "pg_catalog"."default",
  "email" varchar(255) COLLATE "pg_catalog"."default" NOT NULL UNIQUE,
  "created_date" date,
  "last_login_date" date,
  "about_description" varchar(255) COLLATE "pg_catalog"."default",
  "username" varchar(255) COLLATE "pg_catalog"."default" NOT NULL UNIQUE,
  CONSTRAINT "profile_pkey" PRIMARY KEY ("id")
);
ALTER TABLE "profile" OWNER TO "postgres";